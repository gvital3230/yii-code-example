<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $image
 * @property string $image_mob
 * @property string $url
 * @property integer $type
 * @property integer $s3
 */
class Banner extends \yii\db\ActiveRecord
{
    //virtual property for uploaded image
    CONST IMAGE_FOLDER = 'img/banner';
    public $image_file;
    public $image_mob_file;

    const TYPE_HOME_PAGE = 'Home page';
    const TYPE_CONTENT = 'Content';

    const TYPE_HOME_PAGE_INDEX = 1;
    const TYPE_CONTENT_INDEX = 2;

    const CONTENT_SHORT_TAG = '@@BANNER_HERE@@';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * Список доступных типов баннера
     * @return array
     */
    public static function getTypes(){
        return [
            self::TYPE_HOME_PAGE_INDEX => self::TYPE_HOME_PAGE,
            self::TYPE_CONTENT_INDEX => self::TYPE_CONTENT
        ];
    }

    /**
     * Возвращает представление конкретного типа баннера по его коду
     * @param $type
     * @return mixed
     */
    public static function getTypeName($type){
        return self::getTypes()[$type];
    }


    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'common\behaviors\ImageUploaderBehavior',
                'imageConfig' => [
                    // Name of image attribute where the image will be stored
                    'imageAttributes' => [
                        'image',
                    ],
                    // Yii-alias to dir where will be stored subdirectories with images
                    'savePathAlias' => '@frontend/web/' . self::IMAGE_FOLDER,
                    // Yii-alias to root project dir, relative path to the image will exclude this part of the full path
                    'rootPathAlias' => '@frontend/web',
                    // Name of default image. Image placed to: webrooot/images/{noImageBaseName}
                    // You must create all noimage files: noimage.jpg, medium_noimage.jpg, small_noimage.jpg, etc.
                    'noImageBaseName' => 'banner-home.jpg',
                    // List of thumbnails sizes.
                    // Format: [prefix=>max_width]
                    // Thumbnails height calculated proportionally automatically
                    // Prefix '' is special, it determines the max width of the main image
                    'imageSizes' => [
                        '' => 1920,
                        'medium_' => 900,
                    ],
                    // This params will be passed to \yii\validators\ImageValidator
                    'imageValidatorParams' => [
                        'minWidth' => 400,
                        'minHeight' => 300,
                    ],
                    // Cropper config
                    'aspectRatio' => 2.48, // or 16/9(wide) or 1/1(square) or any other ratio. Null - free ratio
                    // default config
                    'imageRequire' => false,
                    'fileTypes' => 'jpg,jpeg,gif,png',
                    'maxFileSize' => 10485760, // 10mb
                    // If backend is located on a subdomain 'admin.', and images are uploaded to a directory
                    // located in the frontend, you can set this param and then getImageSrc() will be return
                    // path to image without subdomain part even in backend part
                    'backendSubdomain' => 'admin.',
                ],
            ],
            'image_mob' => [
                'class' => 'common\behaviors\ImageUploaderBehavior',
                'imageConfig' => [
                    // Name of image attribute where the image will be stored
                    'imageAttributes' => [
                        'image_mob'
                    ],
                    // Yii-alias to dir where will be stored subdirectories with images
                    'savePathAlias' => '@frontend/web/' . self::IMAGE_FOLDER,
                    // Yii-alias to root project dir, relative path to the image will exclude this part of the full path
                    'rootPathAlias' => '@frontend/web',
                    // Name of default image. Image placed to: webrooot/images/{noImageBaseName}
                    // You must create all noimage files: noimage.jpg, medium_noimage.jpg, small_noimage.jpg, etc.
                    'noImageBaseName' => 'banner-home-mob.jpg',
                    // List of thumbnails sizes.
                    // Format: [prefix=>max_width]
                    // Thumbnails height calculated proportionally automatically
                    // Prefix '' is special, it determines the max width of the main image
                    'imageSizes' => [
                        '' => 1000,
                        'medium_' => 900,
                    ],
                    // This params will be passed to \yii\validators\ImageValidator
                    'imageValidatorParams' => [
                        'minWidth' => 400,
                        'minHeight' => 300,
                    ],
                    // Cropper config
                    'aspectRatio' => 1.45, // or 16/9(wide) or 1/1(square) or any other ratio. Null - free ratio
                    // default config
                    'imageRequire' => false,
                    'fileTypes' => 'jpg,jpeg,gif,png',
                    'maxFileSize' => 10485760, // 10mb
                    // If backend is located on a subdomain 'admin.', and images are uploaded to a directory
                    // located in the frontend, you can set this param and then getImageSrc() will be return
                    // path to image without subdomain part even in backend part
                    'backendSubdomain' => 'admin.',
                ],
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['image', 'image_mob', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image' => Yii::t('app', 'Image'),
            'image_mob' => Yii::t('app', 'Image Mob'),
            'url' => Yii::t('app', 'Url'),
            'type' => Yii::t('app', 'Type'),
        ];
    }
}
