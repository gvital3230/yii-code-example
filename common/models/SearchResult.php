<?php

namespace common\models;

use yii\data\ActiveDataProvider;

interface SearchResult
{
    /**
     * Подготовка и конфигурирование запроса для поиска, определение сортировки и представления моделей
     * @param  $searchPhrase string
     * @param  $pagination array
     * @return ActiveDataProvider the loaded model
     */
    public static function searchModels($searchPhrase = null, $pagination = null);

    /**
     * Подготовка и конфигурирование запроса для быстрого поиска, определение сортировки и представления моделей
     * @param  $searchPhrase string
     * @param  $pagination array
     * @return ActiveDataProvider the loaded model
     */
    public static function quickSearchModels($searchPhrase = null, $pagination = null);

    /**
     * Определение ссылки для перехода из результатов поиска
     * @return string
     */
    public function getUrl();

}