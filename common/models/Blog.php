<?php

namespace common\models;

use common\behaviors\ImageUploaderBehavior;
use kartik\helpers\Html;
use v0lume\yii2\metaTags\MetaTagBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the model class for table "blog".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $user_id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property integer $s3
 * @property integer $created_at
 * @property string $slug
 *
 * @property BlogCategory $category
 * @property User $user
 * @property string $frontDescription
 * @property string $url
 */
class Blog extends ActiveRecord implements SearchResult
{
    //virtual property for uploaded image
    CONST IMAGE_FOLDER = 'img/blog';
    public $image_file;
    public $redactor_settings;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * Подготовка и конфигурирование запроса для поиска, определение сортировки и представления моделей
     * @param null $searchPhrase
     * @param null $pagination
     * @return ActiveDataProvider
     */
    public static function searchModels($searchPhrase = null, $pagination = null)
    {
        $query = self::find();
        if ($searchPhrase) {
            $query = $query->andWhere([
                'or',
                ['like', 'title', $searchPhrase],
                ['like', 'description', $searchPhrase]
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($pagination) {
            $dataProvider->setPagination($pagination);
        }

        return $dataProvider;
    }

    /**
     * Подготовка и конфигурирование запроса для быстрого поиска, определение сортировки и представления моделей
     * @param  $searchPhrase string
     * @param  $pagination array
     * @return ActiveDataProvider the loaded model
     */
    public static function quickSearchModels($searchPhrase = null, $pagination = null)
    {
        $query = self::find();
        if ($searchPhrase) {
            $query = $query->andWhere([
                'or',
                ['like', 'title', $searchPhrase],
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($pagination) {
            $dataProvider->setPagination($pagination);
        }

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'user_id', 's3', 'created_at', 'updated_at'], 'integer'],
            [['title', 'image'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => BlogCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'image' => 'Image',
            's3' => 'S3',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(BlogCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function init()
    {
        $this->redactor_settings = [
            'minHeight' => 200,
            'imageUpload' => Url::to(['image-upload']),
            'plugins' => [
                'clips',
                'fullscreen',
                'video',
            ]];

        parent::init();
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            ],
            'MetaTag' => [
                'class' => MetaTagBehavior::className(),
            ],
            TimestampBehavior::className(),
            'image' => [
                'class' => 'common\behaviors\ImageUploaderBehavior',
                'imageConfig' => [
                    // Name of image attribute where the image will be stored
                    'imageAttributes' => [
                        'image',
                    ],
                    // Yii-alias to dir where will be stored subdirectories with images
                    'savePathAlias' => '@frontend/web/' . self::IMAGE_FOLDER,
                    // Yii-alias to root project dir, relative path to the image will exclude this part of the full path
                    'rootPathAlias' => '@frontend/web',
                    // Name of default image. Image placed to: webrooot/images/{noImageBaseName}
                    // You must create all noimage files: noimage.jpg, medium_noimage.jpg, small_noimage.jpg, etc.
                    'noImageBaseName' => 'noimage.jpg',
                    // List of thumbnails sizes.
                    // Format: [prefix=>max_width]
                    // Thumbnails height calculated proportionally automatically
                    // Prefix '' is special, it determines the max width of the main image
                    'imageSizes' => [
                        '' => 600,
                        'medium_' => 900,
                    ],
                    // This params will be passed to \yii\validators\ImageValidator
                    'imageValidatorParams' => [
                        'minWidth' => 400,
                        'minHeight' => 300,
                    ],
                    // Cropper config
                    'aspectRatio' => 1.45, // or 16/9(wide) or 1/1(square) or any other ratio. Null - free ratio
                    // default config
                    'imageRequire' => false,
                    'fileTypes' => 'jpg,jpeg,gif,png',
                    'maxFileSize' => 10485760, // 10mb
                    // If backend is located on a subdomain 'admin.', and images are uploaded to a directory
                    // located in the frontend, you can set this param and then getImageSrc() will be return
                    // path to image without subdomain part even in backend part
                    'backendSubdomain' => 'admin.',
                ],
            ],

        ];
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->user_id = Yii::$app->user->id;
        }
        return parent::beforeSave($insert);
    }

    /**
     * Определение ссылки для перехода из результатов поиска
     * @param bool $scheme
     * @return string
     */
    public function getUrl($scheme = false)
    {
        if ($this->slug) {
            return Url::to([
                'blog/view',
                'id' => $this->slug,
            ], $scheme);
        } else {
            return Url::to([
                'blog/view',
                'id' => $this->id,
            ], $scheme);
        }
    }

    /**
     * Функция интегрирует баннер в текст контента, в месте указанном пользователем (short tag)
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function getFrontDescription()
    {
        /** @var Banner|ImageUploaderBehavior $banner */
        $banner = Banner::find()->andWhere([
            'type' => Banner::TYPE_CONTENT_INDEX
        ])->one();

        $banner_text = '';

        if ($banner) {
            $banner_text = Html::a(
                Html::img($banner->getImageSrc('image'), ['class' => 'img-responsive']),
                $banner->url
            );
//            if ($banner->image_mob) {
//                $banner_text .= Html::a(
//                    Html::img($banner->getImageSrc('image_mob'), ['class' => 'content-banner-mobile']),
//                    $banner->url
//                );
//            }
        }
        return str_replace(Banner::CONTENT_SHORT_TAG, $banner_text, $this->description);
    }
}
