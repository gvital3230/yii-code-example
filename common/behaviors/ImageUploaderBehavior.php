<?php

namespace common\behaviors;

use frostealth\yii2\aws\s3\Storage;
use Yii;
use yii\base\Behavior;
use yii\base\ModelEvent;
use yii\db\ActiveRecord;
use yii\image\drivers\Image_GD;
use yii\image\drivers\Image_Imagick;
use yii\validators\ImageValidator;
use yii\validators\RequiredValidator;
use yii\validators\Validator;
use yii\web\UploadedFile;

/**
 * Поведение для работы с главным изображением материала
 *
 * @package demi\image
 *
 * @property ActiveRecord $owner
 */
class ImageUploaderBehavior extends Behavior
{
    /** @var array Компонент для работы с изображениями (например resize изображений) */
    protected static $_imageComponent;
    /** @var array Конфигурационный массив, который переопределяет вышеуказанные настройки */
    public $imageConfig = [];
    /** @var array название атрибута, хранящего в себе имя изображения или путь к изображению */
    protected $_imageAttributes = ['image'];
    /** @var string альяс директории, куда будем сохранять изображения */
    protected $_savePathAlias = '@frontend/web/images';
    /** @var string альяс корневой директории сайта (где index.php находится) */
    protected $_rootPathAlias = '@frontend/web'; // 10mb
    /** @var string типы файлов, которые можно загружать (нужно для валидации) */
    protected $_fileTypes = 'jpg,jpeg,gif,png';
    /** @var string Максимальный размер загружаемого изображения (байт) */
    protected $_maxFileSize = 10485760;
    /** @var array Размеры изображений, которые необходимо создать после загрузки основного */
    protected $_imageSizes = [];
    /** @var string Имя файла, который будет отображён при условии отсутствия изображения */
    protected $_noImageBaseName = 'noimage.png';
    /** @var boolean Обязательно ли загружать изображение */
    protected $_imageRequire = false;
    /** @var array Дополнительные параметры для ImageValidator */
    protected $_imageValidatorParams = [];
    /** @var string Название субдомена backend`а, нужен для того, чтобы выводить абсолютный путь к изображению в backend`е */
    protected $_backendSubdomain = 'admin.';
    /** @var string Временное хранилище для учёта текущего, уже загруженного изображения */
    protected $_oldImage = null;
    /** @var float|null Соотношение сторон для обрезки изображения, если NULL - свободная область */
    protected $_aspectRatio = null;

    /**
     * @param ActiveRecord $owner
     */
    public function attach($owner)
    {
        parent::attach($owner);

        // Применяем конфигурационные опции
        foreach ($this->imageConfig as $key => $value) {
            $var = '_' . $key;
            $this->$var = $value;
        }

        // Вычисляем корень сайта
        if (empty($this->_rootPathAlias)) {
            $savePathParts = explode('/', $this->_savePathAlias);
            // Удаляем последнюю часть
            unset($savePathParts[count($savePathParts - 1)]);
            // Объединяем все части обратно
            $this->_rootPathAlias = implode('/', $savePathParts);
        }

        // Добавляем валидатор require
        foreach ($this->_imageAttributes as $imageAttribute) {
            if ($this->_imageRequire) {
                $owner->validators->append(Validator::createValidator(RequiredValidator::className(), $owner,
                    $imageAttribute));
            }

            // Подключаем валидатор изображения
            $validatorParams = array_merge([
                'extensions' => $this->_fileTypes,
                'maxSize' => $this->_maxFileSize,
                'skipOnEmpty' => true,
                'tooBig' => 'Image too big, max size: ' . floor($this->_maxFileSize / 1024 / 1024) . ' Мб',
            ], $this->_imageValidatorParams);

            $validator = Validator::createValidator(ImageValidator::className(), $owner, $imageAttribute,
                $validatorParams);
            $owner->validators->append($validator);
        }
    }

    /**
     * Get image config param value
     *
     * @param string $paramName
     *
     * @return mixed
     */
    public function getImageConfigParam($paramName)
    {
        $name = '_' . $paramName;

        return $this->$name;
    }

    /**
     * Set new config param value
     *
     * @param string $paramName
     * @param mixed $value
     */
    public function setImageConfigParam($paramName, $value)
    {
        $name = '_' . $paramName;

        $this->$name = $value;
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
            ActiveRecord::EVENT_AFTER_DELETE => 'deleteAllImages',
        ];
    }

    /**
     * @param ModelEvent $event
     * @return void
     * @throws \yii\base\ErrorException
     */
    public function beforeSave($event)
    {
        $owner = $this->owner;

        foreach ($this->_imageAttributes as $imageAttribute) {
            $image = $owner->{$imageAttribute};
            $image = ($image instanceof UploadedFile) ? $image :
                UploadedFile::getInstance($owner, $imageAttribute . '_file');


            if ($image instanceof UploadedFile) {
                // Если было передано изображение - загружаем его, старое удаляем
                $cropping_data = [];
                if (Yii::$app->request->post($imageAttribute . '-cropping')) {
                    $cropping_data = Yii::$app->request->post($imageAttribute . '-cropping');
                }

                $new_image = static::uploadImage($image, $cropping_data, $imageAttribute);

                if (!empty($new_image)) {
                    $this->deleteImage($imageAttribute);
                    $owner->{$imageAttribute} = $new_image;
                } else {
                    $event->isValid = false;
                }
            } else {
                if ($owner->hasAttribute($imageAttribute . '_base64file')) {
                    $image = $owner->{$imageAttribute . '_base64file'};
                    $cropping_data = [];
                    if ($owner->{$imageAttribute . '_cropping_data'}) {
                        $cropping_data = $owner->{$imageAttribute . '_cropping_data'};
                    }
                    $new_image = static::uploadImageBase64Encoded($image, $cropping_data, $imageAttribute);

                    if (!empty($new_image)) {
                        $this->deleteImage($imageAttribute);
                        $owner->{$imageAttribute} = $new_image;
                    } else {
                        $event->isValid = false;
                    }
                }
            }
        }
    }

    /**
     * Сохраняет файл в соотв. с настройками - локально или в облако
     *
     * @param string | Image_GD|Image_Imagick $source
     * @param string $folder
     * @param string $name
     *
     * @return bool результат успех
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    protected function saveFile($source, $folder, $name)
    {

        $res = false;

        if (\Yii::$app->params['s3enable']) {
            /* @var Storage $storage */
            $this->owner->s3 = 1;

            $storage = \Yii::$app->get('s3bucket');
            $res = $storage->upload($name, $source);
        } else {

            $this->owner->s3 = 0;
            if (is_string($source)) {
                /* @var $source string */
                $res = copy($source, $folder . DIRECTORY_SEPARATOR . $name);

            } else {
                /* @var $file Image_GD|Image_Imagick */
                $res = $source->save($folder . DIRECTORY_SEPARATOR . $name);
            }
        }

        return $res;
    }


    /**
     * Process deletion of image
     * @param $dirName string
     * @param $fileName string
     *
     * @return boolean the status of deletion
     * @throws \yii\base\InvalidConfigException
     */
    public function deleteFile($dirName, $fileName)
    {
        if ($this->owner->s3) {
            $storage = \Yii::$app->get('s3bucket');
            if (\Yii::$app->params['s3enable']) {
                if (!$storage->delete($fileName)) {
                    return false;
                }
            }
        } else {
            $path = $dirName . DIRECTORY_SEPARATOR . $fileName;
            // check if file exists on server
            if (empty($path) || !file_exists($path)) {
                return false;
            }

            // check if uploaded file can be deleted on server
            if (!@unlink($path)) {
                return false;
            }
        }

        return true;
    }

    protected function getImageInfo($image)
    {
        if ($image instanceof UploadedFile) {
            /* @var $image UploadedFile */
            return getimagesize($image->tempName);
        } else {
            /* @var $image Image_GD|Image_Imagick */
            return [
                $image->width,
                $image->height
            ];
        }

    }

    protected function getImportDir()
    {
        $baseDir = realpath(\Yii::getAlias('@runtime')) . '/image-upload';
        if (!is_dir($baseDir)) {
            mkdir($baseDir);
        }

        return $baseDir;

    }

    /**
     * Apply user define crop/rotate settings and save changes in current tempfile
     *
     * @param Image_GD|Image_Imagick $image
     * @param $cropping_data array
     * @param $imageFolder string
     * @param $name string
     *
     * @return Image_GD|Image_Imagick cropped image
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    protected function applyUserCropping($image, array $cropping_data, $imageFolder, $name)
    {

        $x = 0;
        if (array_key_exists('x', $cropping_data)) {
            $x = $cropping_data['x'];
        }
        $y = 0;
        if (array_key_exists('y', $cropping_data)) {
            $y = $cropping_data['y'];
        }
        $width = 0;
        if (array_key_exists('width', $cropping_data)) {
            $width = $cropping_data['width'];
        }
        $height = 0;
        if (array_key_exists('height', $cropping_data)) {
            $height = $cropping_data['height'];
        }
        $rotate = 0;
        if (array_key_exists('rotate', $cropping_data)) {
            $rotate = $cropping_data['rotate'];
        }

        if ($x || $y || $width || $height || $rotate) {

            $image = $image->crop($width, $height, $x, $y);
            if ($rotate) {
                $image = $image->rotate($rotate);
            }

            $this->saveFile($image, $imageFolder, $name);

        }

        return $image;
    }

    /**
     * Apply max width settings and save changes in current tempfile
     *
     * @param Image_GD|Image_Imagick $image
     * @param $maxWidth integer
     * @param $imageFolder string
     * @param $name string
     *
     * @return Image_GD|Image_Imagick cropped image
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    protected function applyMaxWidth($image, $maxWidth, $imageFolder, $name)
    {

        // Reduce image if image is very large
        $imageInfo = $this->getImageInfo($image);
        $img_width = $imageInfo[0];

        if ($img_width > $maxWidth) {
            $image = $image->resize($maxWidth, static::getMaxHeight($maxWidth));
            $this->saveFile($image, $imageFolder, $name);
        }

        return $image;
    }

    /**
     * Apply max width settings and save changes in current tempfile
     *
     * @param Image_GD|Image_Imagick $image
     * @param $width integer
     * @param $imageFolder string
     * @param $name string
     *
     * @return Image_GD|Image_Imagick cropped image
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    protected function applyResize($image, $width, $imageFolder, $name)
    {

        // Reduce image if image is very large
        $imageInfo = $this->getImageInfo($image);
        $img_width = $imageInfo[0];

        $image = $image->resize($width, static::getMaxHeight($width));
        $this->saveFile($image, $imageFolder, $name);

        return $image;
    }


    /**
     * Загружает переданное изображение в нужную директорию
     *
     * @param UploadedFile $image
     * @param array $cropping_data
     *
     * @param $fileAttribute
     * @return string путь к фото для сохранения его в базе, в случае ошибки NULL
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    protected function uploadImage(UploadedFile $image, array $cropping_data, $fileAttribute)
    {
        $DS = DIRECTORY_SEPARATOR;

        $tempFileBaseName = uniqid();
        $tempFileDir = $this->getImportDir();
        $extension = $image->extension;
        if (array_search($extension, explode(',', $this->_fileTypes)) === false) {
            $this->owner->addError($fileAttribute . '_file', 'Uploading files with this extension is not allowed');
            return null;
        }
        $tempFileName = $tempFileBaseName . '.' . $extension;
        $tempFileSource = $tempFileDir . DIRECTORY_SEPARATOR . $image->baseName . '.' . $image->extension;
        $success = $image->saveAs($tempFileSource);
        if (!$success) {
            return null;
        }

        // Save original file
        $name = $tempFileBaseName . '.' . $extension; // Имя будущего файла
        $imageFolder = Yii::getAlias($this->_savePathAlias); // Куда загружать изображение
        $fullImagePath = $imageFolder . $DS . $name; // Полный путь к изображению

        $this->saveFile($tempFileSource, $imageFolder, $tempFileBaseName . '_original.' . $extension);

        //user cropping
        /* @var $image Image_GD|Image_Imagick */
        $imageComponent = static::getImageComponent();
        $image = $imageComponent->load($tempFileSource);

        $image = $this->applyUserCropping($image, $cropping_data, $tempFileDir, $tempFileName);
        $image = $this->applyMaxWidth($image, 2500, $tempFileDir, $tempFileName);

        // Если изображение успешно сохранено - делаем ресайзные копии
        $sizes = $this->getImageSizes();
        foreach ($sizes as $key => $size) {
            $this->applyResize($image, $size, $imageFolder, $key . $tempFileName);
        }


        //clean temp file
        @unlink($tempFileSource);

        return $tempFileName;

    }

    /**
     * Загружает переданное изображение в нужную директорию
     *
     * @param string $image
     * @param array $cropping_data
     *
     * @param $imageAttribute
     * @return string путь к фото для сохранения его в базе, в случае ошибки NULL
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    protected function uploadImageBase64Encoded($image, array $cropping_data, $imageAttribute)
    {
        if (!$image) {
            return null;
        }

        list(, $image) = explode(';', $image);
        list(, $image) = explode(',', $image);
        $image = base64_decode($image);

        if (!$image) {
            return null;
        }

        $f = finfo_open();

        $mime_type = explode('/', finfo_buffer($f, $image, FILEINFO_MIME_TYPE));
        $extension = $mime_type[1];

        if (array_search($extension, explode(',', $this->_fileTypes)) === false) {
            $this->owner->addError($imageAttribute, 'Uploading files with this extension is not allowed');
            return null;
        }


        $DS = DIRECTORY_SEPARATOR;

        $tempFileBaseName = uniqid();
        $tempFileDir = $this->getImportDir();
        $tempFileName = $tempFileBaseName . '.' . $extension;
        $tempFileSource = $tempFileDir . DIRECTORY_SEPARATOR . $tempFileBaseName . '.' . $extension;
        $success = file_put_contents($tempFileSource, $image);
        if (!$success) {
            return null;
        }

        // Save original file
        $name = $tempFileBaseName . '.' . $extension; // Имя будущего файла
        $imageFolder = Yii::getAlias($this->_savePathAlias); // Куда загружать изображение
        $fullImagePath = $imageFolder . $DS . $name; // Полный путь к изображению

        $this->saveFile($tempFileSource, $imageFolder, $tempFileBaseName . '_original.' . $extension);

        //user cropping
        /* @var $image Image_GD|Image_Imagick */
        $imageComponent = static::getImageComponent();
        $image = $imageComponent->load($tempFileSource);

        $image = $this->applyUserCropping($image, $cropping_data, $tempFileDir, $tempFileName);
        $image = $this->applyMaxWidth($image, 2500, $tempFileDir, $tempFileName);

        // Если изображение успешно сохранено - делаем ресайзные копии
        $sizes = $this->getImageSizes();
        foreach ($sizes as $key => $size) {
            $this->applyResize($image, $size, $imageFolder, $key . $tempFileName);
        }


        //clean temp file
        @unlink($tempFileSource);

        return $tempFileName;

    }

    public function regenerateImage($imageAttribute)
    {
        $owner = $this->owner;
        $root = Yii::getAlias($this->_rootPathAlias); // Корень сайта
        $image_name = $owner->{$imageAttribute};

        if (!$image_name) {
            return;
        }

        $res = $root . $this->getImageSrc($imageAttribute);

        if (file_exists($res)) {
            /* @var $image Image_GD|Image_Imagick */
            $imageComponent = static::getImageComponent();
            $image = $imageComponent->load($res);

            $imageFolder = Yii::getAlias($this->_savePathAlias); // Куда загружать изображение

            $sizes = $this->getImageSizes();
            foreach ($sizes as $key => $size) {
                $this->applyResize($image, $size, $imageFolder, $key . $image_name);
            }

        }

    }

    /**
     * Get image component
     *
     * @return \yii\image\ImageDriver
     * @throws \yii\base\InvalidConfigException
     */
    public static function getImageComponent()
    {
        // Получаем компонент для работы с изображениями
        if (is_object(static::$_imageComponent)) {
            $imageComponent = static::$_imageComponent;
        } else {
            $imageComponent = Yii::createObject([
                'class' => 'yii\image\ImageDriver',
            ]);
            // Сохраняем компонент для последующей работы с ним
            static::$_imageComponent = $imageComponent;
        }

        return $imageComponent;
    }

    /**
     * Возвращает максимальную высоту изображения относительно переданной ширины
     *
     * @param integer $width
     *
     * @return integer
     */
    public static function getMaxHeight($width)
    {
        return $width * 2;
    }

    /**
     * Get image sizes
     *
     * @return array
     */
    protected function getImageSizes()
    {
        if (is_callable($this->_imageSizes)) {
            return call_user_func($this->_imageSizes, $this->owner);
        } else {
            return $this->_imageSizes;
        }
    }

    /**
     * Подставляет префикс к имени файла
     * Например addPrefixToFile("dirname/50b3d1ad130d0.png", "normal_") вернёт "dirname/normal_50b3d1ad130d0.png"
     *
     * @param string $path путь к главному изображению
     * @param string $prefix префикс нужного размера
     *
     * @return string путь с префиксом
     */
    public static function addPrefixToFile($path, $prefix = null)
    {
        if ($prefix === null || $prefix == '') {
            return $path;
        }

        $path = str_replace('\\', '/', $path);
        $dir = explode('/', $path);
        $lastIndex = count($dir) - 1;
        if ('original' == $prefix) {
            $filename = explode('.', $dir[$lastIndex]);
            $dir[$lastIndex] = $filename[0] . '_' . $prefix . '.' . $filename[1];
        } else {
            $dir[$lastIndex] = $prefix . $dir[$lastIndex];
        }


        return implode('/', $dir);
    }

    /**
     * Delete image file and set|save model image field to null
     *
     * @param string $imageAttribute current image attribute
     * @param bool $updateDb need to update image field in DB
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function deleteImage($imageAttribute, $updateDb = false)
    {
        $owner = $this->owner;
        $DS = DIRECTORY_SEPARATOR;

        $image = str_replace('/', $DS, $owner->{$imageAttribute});

        if (!empty($image)) {
            $dirName = Yii::getAlias($this->_savePathAlias);
            // Удаляем все ресазы изображения
            foreach ($this->getImageSizes() as $prefix => $size) {
                $this->deleteFile($dirName, static::addPrefixToFile($image, $prefix));
            }
            // Remove original image
            $this->deleteFile($dirName, static::addPostfixToFile($image, '_original'));
        }

        // Обнуляем значение
        $owner->{$imageAttribute} = null;
        $this->_oldImage = null;

        if ($updateDb) {
            $owner->update(false, [$imageAttribute]);
        }

    }

    /**
     * Delete image file and set|save model image field to null
     *
     * @param $event
     * @param bool $updateDb need to update image field in DB
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function deleteAllImages($event, $updateDb = false)
    {
        foreach ($this->_imageAttributes as $imageAttribute) {
            $this->deleteImage($imageAttribute, $updateDb);
        }
    }

    /**
     * Return server path to original image src
     *
     * @return string
     */
    protected function getOriginalImagePath($image_name)
    {
        $savePath = Yii::getAlias($this->_savePathAlias);

        $res = static::addPostfixToFile($savePath . DIRECTORY_SEPARATOR . $image_name, '_original');
        return $res;
    }

    /**
     * Подставляет постфикс к имени файла
     * Например addPrefixToFile("dirname/50b3d1ad130d0.png", "_normal") вернёт "dirname/50b3d1ad130d0_normal.png"
     *
     * @param string $path путь к главному изображению
     * @param string $postfix постфикс нужного размера
     *
     * @return string путь с постфиксом
     */
    public static function addPostfixToFile($path, $postfix = null)
    {
        if ($postfix === null || $postfix == '') {
            return $path;
        }

        $parts = explode('.', $path);

        if (count($parts) === 1) {
            return $postfix . $path;
        }

        $parts[count($parts) - 2] .= $postfix;

        return implode('.', $parts);
    }

    private function getNoImageBaseName($imageAttribute)
    {
        foreach ($this->owner->behaviors() as $behavior) {
            if (!is_array($behavior)) {
                continue;
            }
            if (!($behavior['class'] == ImageUploaderBehavior::className())) {
                continue;
            }

            if (in_array($imageAttribute, $behavior['imageConfig']['imageAttributes'])) {
                return $behavior['imageConfig']['noImageBaseName'];
            }
        }

        return $this->_noImageBaseName;
    }

    /**
     * Возвращает путь к картинке этой модели указанного размера
     *
     * @param string $size Префикс размера нужного изображения
     *
     * @return string путь к изображению, пригодный для Html::image()
     * @throws \yii\base\InvalidConfigException
     */
    public function getImageSrc($imageAttribute, $size = null)
    {
        $owner = $this->owner;

        $prefix = '';
        if (Yii::$app->request instanceof \yii\web\Request) {
//            $prefix = Yii::$app->request->baseUrl;
            $prefix = '';
            $host = Yii::$app->request->hostInfo;
            // Если мы сейчас находимся на субдомене admin.*, то вернём абсолютный путь к картинке на frontend
            if (!empty($this->_backendSubdomain) && strpos($host, $this->_backendSubdomain)) {
                $prefix = str_replace($this->_backendSubdomain, '', $host) . $prefix;
                $prefix = str_replace($this->_backendSubdomain, '', $host) . $prefix;
            }
        }

        $image = $owner->{$imageAttribute};

        if (empty($image)) {

            if (isset($this->getImageSizes()[$size])) {
                return $prefix . '/img/' . static::addPrefixToFile($this->getNoImageBaseName($imageAttribute), $size);
            }

            return $prefix . '/img/' . $this->getNoImageBaseName($imageAttribute);
        }

        $root = Yii::getAlias($this->_rootPathAlias); // Корень сайта
        $path = Yii::getAlias($this->_savePathAlias); // Получаем путь до папки с загрузками
        $path = str_replace($root, '', $path); // Убиаем из полного пути часть webroot
        $path = str_replace('\\', '/', $path); // Заменяем "\" на "/"
        $folder = $prefix . '/' . trim($path, '/') . '/';

        if ($this->owner->s3) {
            /* @var Storage $storage */
            $storage = \Yii::$app->get('s3bucket');
            if (!empty($size)) {
                $res = $storage->getCdnUrl(static::addPrefixToFile($image, $size));
            } else {
                $res = $storage->getCdnUrl($image);
            }
        } else {
            if (!empty($size)) {
                $res = $folder . static::addPrefixToFile($image, $size);
            } else {
                $res = $folder . $image;
            }
        }

        return $res;
    }

    function startsWith($haystack, $needle)
    {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    /**
     * Return instance of current behavior
     *
     * @return self $this
     */
    public function geImageBehavior()
    {
        return $this;
    }

}