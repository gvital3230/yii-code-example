<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 7/8/16
 * Time: 3:45 PM
 */

namespace common\components;


use cornernote\returnurl\ReturnUrl;

/**
 * Class ReturnUrlCustom
 * @package common\components
 * Хелпер работы с адресами обратной переадресации
 */
class ReturnUrlCustom extends ReturnUrl
{
    /**
     * Добавляет токен текущего адреса в переданный массив параметров
     * @param $array
     * @return array
     */
    public static function addRequestKeyOption($array)
    {
        $token = self::getRequestToken();
        if (!$token) {
            return $array;
        } else {
            return array_merge($array, [
                self::$requestKey => $token
            ]);
        }
    }

}