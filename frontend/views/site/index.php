<?php
/**
 * @var $this yii\web\View
 * @var $blog \common\models\Blog
 **/

$this->title = 'My Yii Application';
?>
<div class="container">
    <div class="row">
        <div class="col">
            <h1><?= $blog->title ?></h1>
            <p class="lead"><?= $blog->frontDescription ?></p></div>
    </div>
</div>

