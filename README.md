**This is not real production application**, but the simple example of my code style/model patterns at YII2 framework.
The features which was implemented here:

* Banner model, which consists of image, url and some classification
* Blog post, which also have title, description
* Blog post description can be edited by WYSIWYG editor
* Images can be manually cropped by user during uploading. Application automatically generates optimized version of image for all predefined resolutions
* User can embed banner in his blog posts by using some short code 

Live version of this application can be found [here](https://yii-code-example.gvital.xyz)

Username: `admin`
Password: `password`