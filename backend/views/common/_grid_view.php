<?php

use kartik\grid\GridView;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $searchModel common\models\UserSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$content_toolbar = '';
$need_confirmation = false;

if(isset($sms)){
    $content_toolbar .= Html::a(Yii::t('app', 'Send sms'), $sms,
        [
            'data-pjax' => 0,
            'class' => 'btn btn-info',
            'data-toggle' => 'modal',
            'data-target' => '#smsModal',
            'title' => Yii::t('app', 'Sms')
        ]);
}

if (isset($import_url)) {
    $content_toolbar .= Html::a(Yii::t('app', 'Импорт товаров из файла .csv'), $import_url,
        [
            'data-pjax' => 0,
            'class' => 'btn btn-primary',
            'title' => Yii::t('app', 'Импорт')
        ]);
}

if (isset($create_url)) {
    $content_toolbar .= Html::a('<i class="glyphicon glyphicon-plus"></i>', $create_url,
        [
            'data-pjax' => 0,
            'class' => 'btn btn-success',
            'title' => Yii::t('app', 'Add new')
        ]);
}
if (isset($delete_selected_url)) {
    $need_confirmation = true;
    $onclick = '
var selected_keys = $(".grid-view").yiiGridView("getSelectedRows");
$.ajax({
    type: "POST",
    url: "' . $delete_selected_url . '",
    data: {
    keys: selected_keys
    }
});';
    $content_toolbar .= Html::button('<i class="glyphicon glyphicon-ok-circle"></i>',
        [
            'data-pjax' => 0,
            'data-onclick' => $onclick,
            'data-toggle' => 'modal',
            'data-target' => '#confirm-delete',
            'data-modal-body' => Yii::t('app', 'Будут удалены ВЫБРАННЫЕ строки и вся связанная информация, включая изображения, отзывы и т.д.'),
            'class' => 'btn btn-warning',
            'title' => Yii::t('app', 'Удалить выбранные')
        ]);
}

if (isset($delete_all_url)) {
    $need_confirmation = true;
    $onclick = '
$.ajax({
    type: "POST",
    url: "' . $delete_all_url . '",
});';

    $content_toolbar .= Html::button('<i class="glyphicon glyphicon-trash"></i>',
        [
            'data-pjax' => 0,
            'data-onclick' => $onclick,
            'data-toggle' => 'modal',
            'data-target' => '#confirm-delete',
            'data-modal-body' => Yii::t('app', 'Будут удалены все строки и вся связанная информация, включая изображения, отзывы и т.д.'),
            'class' => 'btn btn-danger',
            'title' => Yii::t('app', 'Удалить все')
        ]);
}

$toolbar = [
    [
        'content' => $content_toolbar
    ],
    '{export}',
    '{toggleData}'
];

if (!isset($disable_pjax)) {
    $pjax = true;
} else {
    $pjax = !$disable_pjax;
}

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'pjax' => $pjax,
    // set your toolbar
    'toolbar' => $toolbar,
    // set export properties
    'export' => [
        'fontAwesome' => true
    ],
    // parameters from the demo form
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'showPageSummary' => false,
    'panel' => [
        'type' => GridView::TYPE_DEFAULT,
    ],
]); ?>
<?php if ($need_confirmation): ?>
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h2>Требуется подтверждение!</h2>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger btn-ok">Delete</button>
                </div>
            </div>
        </div>
    </div>
    <?php $this->registerJsFile(Yii::$app->request->baseUrl . '/js/confirmation_popup.js', ['depends' => [\yii\web\JqueryAsset::class]]); ?>
<?php endif; ?>