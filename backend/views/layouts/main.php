<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);

$js =
    <<< 'JS'
/* To initialize BS3 tooltips set this below */
$('body').tooltip({
    selector: "[data-toggle='tooltip']"
});
// $(function () { 
//     $("[data-toggle='tooltip']").tooltip(); 
// });
/* To initialize BS3 popovers set this below */
//$(function () { 
//    $("[data-toggle='popover']").popover(); 
//});
JS;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Back to public zone',
        'brandUrl' => '/',
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        if (Yii::$app->user->id == 1) {
            $menuItems = [
//                [
//                    'label' => Yii::t('app', 'Locations'),
//                    'items' => [
//                        ['label' => Yii::t('app', 'Locations'), 'url' => ['/location/index']],
//                        '<li class="divider"></li>',
//                        ['label' => Yii::t('app', 'Location reviews'), 'url' => ['/location-review/index']],
//                        ['label' => Yii::t('app', 'Deals'), 'url' => ['/deal/index']],
//                        '<li class="divider"></li>',
//                        ['label' => Yii::t('app', 'Regions'), 'url' => ['/location-region/index']],
//                        ['label' => Yii::t('app', 'Menu item types'), 'url' => ['/menu-item-type/index']],
//                        '<li class="divider"></li>',
//                        ['label' => Yii::t('app', 'Preorders'), 'url' => ['/preorder/index']],
//                    ]
//                ],
//                [
//                    'label' => Yii::t('app', 'Shop'),
//                    'items' => [
//                        ['label' => Yii::t('app', 'Shop items'), 'url' => ['/shop-item/index']],
//                        ['label' => Yii::t('app', 'Orders'), 'url' => ['/order/index']],
//                        '<li class="divider"></li>',
//                        ['label' => Yii::t('app', 'Coupons'), 'url' => ['/coupon/index']],
//                        ['label' => Yii::t('app', 'Sales'), 'url' => ['/discount/index']],
//                        ['label' => Yii::t('app', 'Categories'), 'url' => ['/shop-item-category/index']],
//                        ['label' => Yii::t('app', 'Item reviews'), 'url' => ['/shop-item-review/index']],
//                        ['label' => Yii::t('app', 'Suppliers'), 'url' => ['/supplier/index']],
//                        '<li class="divider"></li>',
//                        ['label' => Yii::t('app', 'Sales with suppliers'), 'url' => ['/report/sales-with-suppliers']],
//                    ]
//                ],
//                [
//                    'label' => Yii::t('app', 'Strains'),
//                    'items' => [
//                        ['label' => Yii::t('app', 'Strains'), 'url' => ['/strain/index']],
//                        '<li class="divider"></li>',
//                        ['label' => Yii::t('app', 'Properties'), 'url' => ['/property/index']],
//                        ['label' => Yii::t('app', 'Strain reviews'), 'url' => ['/strain-review/index']],
//                    ]
//                ],
//                [
//                    'label' => Yii::t('app', 'Medical ID'),
//                    'items' => [
//                        ['label' => Yii::t('app', 'Doctors'), 'url' => ['/doctor/index']],
//                        ['label' => Yii::t('app', 'Medical states'), 'url' => ['/medical-state/index']],
//                        '<li class="divider"></li>',
//                        ['label' => Yii::t('app', 'Medical state FAQ'), 'url' => ['/medical-faq/index']],
//                    ]
//                ],
//                [
//                    'label' => Yii::t('app', 'Events'),
//                    'items' => [
//                        ['label' => Yii::t('app', 'Events'), 'url' => ['/event/index']],
//                        '<li class="divider"></li>',
//                        ['label' => Yii::t('app', 'Event states'), 'url' => ['/event-state/index']],
//                    ]
//                ],
                [
                    'label' => Yii::t('app', 'Blogs'),
                    'items' => [
                        ['label' => Yii::t('app', 'Blog posts'), 'url' => ['/blog/index']],
                        ['label' => Yii::t('app', 'Blog categories'), 'url' => ['/blog-category/index']],
                    ]
                ],
//                [
//                    'label' => Yii::t('app', 'SMS'),
//                    'items' => [
//                        ['label' => Yii::t('app', 'SMS messages'), 'url' => ['/sms-message/index']],
//                        ['label' => Yii::t('app', 'SMS usage'), 'url' => ['/report/sms-usage']],
//                    ]
//                ],
                ['label' => Yii::t('app', 'Banners'), 'url' => ['/banner/index']],
            ];
        }

        $menuItems[] = [
            'label' => Yii::t('app', 'Logout') . ' (' . Yii::$app->user->identity->username . ')',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
