<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BlogCategory */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'BlogCategory',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'BlogCategory'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
