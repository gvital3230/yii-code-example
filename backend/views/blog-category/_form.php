<?php


use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BlogCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<?
$form = ActiveForm::begin(
    [
        'type' => ActiveForm::TYPE_VERTICAL
    ]
);
?>

<div class="panel panel-default">
    <div class="panel-heading"><?= Yii::t('app', 'Location review') ?></div>
    <div class="panel-body">
        <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [       // 2 column layout
                'title' => [
                    'type' => Form::INPUT_TEXT,
                ],
            ],
        ]);
        ?>
    </div>
</div>
<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
