<?php


/* @var $this yii\web\View */
/* @var $model common\models\Blog */

$this->title = Yii::t('app', 'Create follower entry');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blog'), 'url' => ['index']];
?>
<div class="model-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
