<?php


use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\web\JsExpression;
use \kartik\file\FileInput;
use \kartik\depdrop\DepDrop;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Blog */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$form = ActiveForm::begin(
    [
        'type' => ActiveForm::TYPE_VERTICAL,
        'options' => ['enctype' => 'multipart/form-data'] // important
    ]
);

?>
<div class="panel panel-info">
    <div class="panel-heading">
        <p>
            To render banner put this shortcut in any place of your
            content: <?= \common\models\Banner::CONTENT_SHORT_TAG ?>
        </p>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-body">

        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                'image' => [
                    'label' => false,
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\common\widgets\upload\UploadCrop',
                    'options' =>
                        [
                            'form' => $form,
                            'model' => $model,
                            'attribute' => 'image',
                            'jcropOptions' => [
                                'dashed' => true,
                                'zoomable' => true,
                                'rotatable' => true,
                                'aspectRatio' => 1.45,
                            ]
                        ],
                ],
                'category_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\kartik\widgets\Select2',
                    'options' =>
                        [
                            'data' => \yii\helpers\ArrayHelper::map(\common\models\BlogCategory::find()->all(), 'id', 'title'),
                        ],
                ],
                'title' => [
                    'type' => Form::INPUT_TEXT,
                ],
                'slug' => [
                    'type' => Form::INPUT_TEXT,
                ],
                'description' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\vova07\imperavi\Widget',
                    'options' => [
                        'settings' => $model->redactor_settings,
                    ],
                ]
            ]
        ]);
        ?>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">Meta tags</div>
    <div class="panel-body">
        <?php
        echo \v0lume\yii2\metaTags\MetaTags::widget([
            'model' => $model,
            'form' => $form
        ]);
        ?>
    </div>
</div>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

