<?php

/* @var $this yii\web\View */
/* @var $model common\models\Blog */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Blog',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blog'), 'url' => ['index']];
?>
<div class="model-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
