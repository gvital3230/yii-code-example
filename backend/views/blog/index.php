<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BlogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Blogs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'user.username',
        [
            'attribute' => 'category.title',
            'label' => 'Category'
        ],
        'title',
        'created_at:date',
        [
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{update} {delete}',
            'urlCreator' => function ($action, $model, $key, $index) {
                $token = \common\components\ReturnUrlCustom::getToken();
                return Url::to([
                    'blog/' . $action,
                    'id' => $model->id,
                    'ru' => $token
                ]);
            },
        ]];

    $token = \common\components\ReturnUrlCustom::getToken();
    $create_url = Url::to([
        'create',
        'ru' => $token,
    ]);
    echo $this->render('/common/_grid_view', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'gridColumns' => $gridColumns,
        'create_url' => $create_url,
    ]);
    ?>

</div>
