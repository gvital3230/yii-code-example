<?php


/* @var $this yii\web\View */
/* @var $model common\models\Banner */

$this->title = Yii::t('app', 'Create follower entry');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Banner'), 'url' => ['index']];
?>
<div class="model-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
