<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Banners');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'format' => 'html',
            'value' => function ($model){
                /* @var \common\models\Banner | \common\behaviors\ImageUploaderBehavior $model*/
                return Html::img($model->getImageSrc('image'), ['width'=>'300']);
            },
        ],
        [
            'format' => 'html',
            'value' => function ($model){
                /* @var \common\models\Banner | \common\behaviors\ImageUploaderBehavior $model*/
                return Html::img($model->getImageSrc('image_mob'), ['width'=>'300']);
            },
        ],
        'url',
        [
            'attribute' => 'type',
            'vAlign' => 'middle',
            'width' => '180px',
            'format' => 'raw',
            'value' => function ($model){
                /* @var \common\models\Banner | \common\behaviors\ImageUploaderBehavior $model*/
                return \common\models\Banner::getTypeName($model->type);
            },
            'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
            'filter' => \common\models\Banner::getTypes(),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Any type'],
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{update} {delete}',
            'urlCreator' => function ($action, $model, $key, $index) {
                $token = \common\components\ReturnUrlCustom::getToken();
                return Url::to([
                    'banner/' . $action,
                    'id' => $model->id,
                    'ru' => $token
                ]);
            },
        ]
    ];

    $token = \common\components\ReturnUrlCustom::getToken();
    $create_url = Url::to([
        'create',
        'ru' => $token,
    ]);
    echo $this->render('/common/_grid_view', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'gridColumns' => $gridColumns,
        'create_url' => $create_url,
    ]);
    ?>

</div>
