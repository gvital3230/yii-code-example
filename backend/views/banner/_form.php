<?php


use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\web\JsExpression;
use \kartik\file\FileInput;
use \kartik\depdrop\DepDrop;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Banner */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$form = ActiveForm::begin(
    [
        'type' => ActiveForm::TYPE_VERTICAL,
        'options' => ['enctype' => 'multipart/form-data'] // important
    ]
);

?>

<div class="panel panel-default">
    <div class="panel-body">

        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                'image' => [
                    'label' => false,
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\common\widgets\upload\UploadCrop',
                    'options' =>
                        [
                            'form' => $form,
                            'model' => $model,
                            'attribute' => 'image',
                            'jcropOptions' => [
                                'dashed' => true,
                                'zoomable' => true,
                                'rotatable' => true,
                                'aspectRatio' => 2.48,
                            ]
                        ],
                ],
                'image_mob' => [
                    'label' => false,
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\common\widgets\upload\UploadCrop',
                    'options' =>
                        [
                            'form' => $form,
                            'model' => $model,
                            'attribute' => 'image_mob',
                            'jcropOptions' => [
                                'dashed' => true,
                                'zoomable' => true,
                                'rotatable' => true,
                                'aspectRatio' => 1.45,
                            ]
                        ],
                ],
                'type' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\kartik\widgets\Select2',
                    'options' =>
                        [
                            'data' => \common\models\Banner::getTypes(),
                        ],
                ],
                'url' => [
                    'type' => Form::INPUT_TEXT,
                ],
            ]
        ]);
        ?>
    </div>
</div>
<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

