$('#confirm-delete').on('show.bs.modal', function (e) {
    $(this).find('.btn-ok').attr('onclick', $(e.relatedTarget).data('onclick'));
    $(this).find('.modal-body').text($(e.relatedTarget).data('modal-body'));
});