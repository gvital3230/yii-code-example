<?php

use yii\db\Migration;

/**
 * Class m180916_081343_blog_init
 */
class m180916_081343_blog_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%blog_category}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
        ], $tableOptions);

        $this->createTable('{{%blog}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'user_id' => $this->integer(),
            'title' => $this->string(),
            'description' => $this->text(),
            'image' => $this->string(),
            'slug' => $this->string(),
            's3' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);


        $this->addForeignKey('fk_blog_category', '{{%blog}}', 'category_id', '{{%blog_category}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_blog_user', '{{%blog}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_blog_category', '{{%blog}}');
        $this->dropForeignKey('fk_blog_user', '{{%blog}}');

        $this->dropTable('{{%blog_category}}');

        $this->dropTable('{{%blog}}');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180916_081343_blog_init cannot be reverted.\n";

        return false;
    }
    */
}
