<?php

use yii\db\Migration;

/**
 * Class m180912_151513_banner
 */
class m180912_151513_banner extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%banner}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string(),
            'image_mob' => $this->string(),
            'url' => $this->string(),
            'type' => $this->integer()->defaultValue(0),
            's3' => $this->boolean()->defaultValue(0)
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%banner}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180912_151513_banner cannot be reverted.\n";

        return false;
    }
    */
}
